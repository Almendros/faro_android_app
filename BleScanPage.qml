
import QtQuick 2.6
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.1


Pane {
    id: idblescanpage
        property var itemstack
    property bool busy
    property string ipw
    property var delegateComponentMap: {
        "ItemDelegate": itemDelegateComponent,
    }
    property bool deviceState: device.state
    property string deviceUpdate: device.update
    property bool newDeviceFound : device.newdevice
    property var servicesList : device.servicesList
    property var characteristicList : device.characteristicList
    property int nelementsfound: 0
    property string nservicio
//    property string ncarac
//    property string nnotify
    property int deviceselected
    property int devavisstate: device.devavisstate
    onDeviceStateChanged:
    {
        if (!device.state)
        {
            lblscanstate.text = "Scan Finished";
            busylegeneral.running = false;
            var list = device.devicesList
            var keysArray = Object.keys(list);
            var longitud = keysArray.length;
            for (var j = 0; j < longitud; j++)
            {
                var str = j.toString();
                var obj = list[str];
                for (var prop in obj)
                {
                    if (prop === "deviceName")
                    {
                        petsModel.insert(j, {"text": obj[prop], "ebusy": false, "eipw": ""});
                    }
                }
            }
        }
        console.log(device.state);
    }
    onDeviceUpdateChanged:
    {
        console.log(device.update);
        //petsModel.insert(nelementsfound, {"text": modelData.deviceName, "ebusy": false, "eipw": ""});
        //nelementsfound = nelementsfound + 1;
    }
    onNewDeviceFoundChanged:
    {
        //device.setNewDevice(false);
        //ias petsModel.insert(nelementsfound, {"text": modelData.deviceName, "ebusy": false, "eipw": ""});
        // console.log(device.d);
    }
    onServicesListChanged:
    {
        var list = device.servicesList
        var keysArray = Object.keys(list);
        var longitud = keysArray.length;
        for (var j = 0; j < longitud; j++)
        {
            var str = j.toString();
            var obj = list[str];
            for (var prop in obj)
            {
                console.log(prop + ":" + obj[prop]);
                //if (prop === "serviceUuid" && obj[prop] === "e91677e0-f036-45de-b3e3-953583e49a61")
                if (prop === "serviceUuid" && obj[prop] === "4880c12c-fdcb-4077-8920-a450d7f9b907")
                {
                    device.connectToService(obj[prop]);
                    nservicio = obj[prop];
                    break;
                }
            }
        }
    }
    onCharacteristicListChanged:
    {
        var list = device.characteristicList
        var keysArray = Object.keys(list);
        var longitud = keysArray.length;
        var servicerdy = 0;
        var ncarac = ""
        var nnotify = ""

        for (var j = 0; j < longitud; j++)
        {
            var str = j.toString();
            var obj = list[str];
            for (var prop in obj)
            {
                console.log(prop + ":" + obj[prop]);
//                if (prop === "characteristicUuid" && obj[prop] === "e91677e2-f036-45de-b3e3-953583e49a61")
//                {
//                    ncarac = obj[prop];
//                    //device.writeData(nservicio, ncarac);
//                    servicerdy += 1;
//                }
                if (prop === "characteristicUuid" && obj[prop] === "fec26ec4-6d71-4442-9f81-55bc21d658d6")
                //else if (prop === "characteristicUuid" && obj[prop] === "e91677e1-f036-45de-b3e3-953583e49a61")
                {
                    nnotify = obj[prop]; ncarac = obj[prop];
                   // device.writeData(nservicio, ncarac);
                    servicerdy += 1;
                }

            }
        }
        if (servicerdy)
        {
            petsModel.set(deviceselected, {"ebusy": false});
            itemstack = stackView.push("PageLucidDemo.qml");
            //device.writeConnect2AvisadorNotif(nservicio, ncarac, nnotify);
        }
    }
    onDevavisstateChanged:
    {
        console.log(devavisstate);
       // device.updateAndroidNotification();
        switch(devavisstate)
        {
          case 0:
            petsModel.set(deviceselected, {"ebusy": false, "eipw": "No Answer"});
            break;
          case 1:
             petsModel.set(deviceselected, {"ebusy": false, "eipw": "Out+Red"});
             break;
          case 2:
            petsModel.set(deviceselected, {"ebusy": false, "eipw": "In+Red"});
            break;
          case 3:
            petsModel.set(deviceselected, {"ebusy": false, "eipw": "Out+Green"});
            break;
          case 4:
            petsModel.set(deviceselected, {"ebusy": false, "eipw": "In+Green"});
            break;
          case 5:
            petsModel.set(deviceselected, {"ebusy": false, "eipw": "Disconnected"});
            break;
          case 6:
            petsModel.set(deviceselected, {"ebusy": false, "eipw": "No Gatt"});
            break;

        }
    }

    Label
    {
        id: lblscanstate
        width: 200
        wrapMode: Label.Wrap
        horizontalAlignment: Qt.AlignHCenter
        //text: device.update
        anchors.top: parent.top
        anchors.topMargin: 25
        anchors.left: parent.left
        anchors.leftMargin: 120
        font.pixelSize: 12
        font.bold: false
    }
    Button
    {
        id: button
        text: qsTr("Start Discovery")
        anchors.top: parent.top
        anchors.topMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 20
        onClicked:
        {
            device.startDeviceDiscovery();
            if (device.state)
            {
                petsModel.clear();
                lblscanstate.text = "Searching...";
                lblscanstate.visible = true;
                busylegeneral.running = true;
            }
        }
    }
    BusyIndicator
    {
        id: busylegeneral
        width: 40
        height: 40
        anchors.top: parent.top
        anchors.topMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 250
        running : false
    }
    Component
    {
        id: itemDelegateComponent

        ItemDelegate
        {
            id: itemDelegate
            text: labelText
            font.pixelSize: 14
            font.bold: true
            width: parent.width
            BusyIndicator
            {
                id: indisingle
                width: 40
                height: 40
                anchors.left: parent.left
                anchors.leftMargin: 190
                running: busy
            }
            Label {
                id: ipwifi
                width: 200
                wrapMode: Label.Wrap
                horizontalAlignment: Qt.AlignHCenter
                text: ipw
                anchors.top: parent.top
                anchors.topMargin: 15
                anchors.left: parent.left
                anchors.leftMargin: 200
                font.pixelSize: 12
                font.bold: false
            }
            onClicked:
            {
                for (var j = 0; j < view.count; j++)
                {
                    if (petsModel.get(j).text === itemDelegate.text)
                    {
                        console.log(j);
                        deviceselected = j;
                        petsModel.set(j, {"ebusy": true, "eipw": "Connecting.."});
                        var str = j.toString();
                        var list = device.devicesList
                        var obj = list[str];
                        for (var prop in obj)
                        {
                            if (prop === "deviceAddress")
                            {
                                device.scanServices(obj[prop]);
                            }
                        }

                        break;
                    }
                }
            }

        }
    }//component
    ColumnLayout
    {
        id: column
        spacing: 40
        anchors.fill: parent
        anchors.topMargin: 60

        ListView {
            id: listView
            Layout.fillWidth: true
            Layout.fillHeight: true
            clip: true
            //            model: device.devicesList
            model: ListModel {
                id: petsModel
                //ListElement { type: "ItemDelegate"; text: "ItemDelegate1" ; ebusy: true}
                //                ListElement { type: "ItemDelegate"; text: "ItemDelegate2" ; ebusy: true}
                //                ListElement { type: "ItemDelegate"; text: "ItemDelegate3" ; ebusy: true}
            }

            section.property: "type"
            section.delegate: Pane {
                width: listView.width
            }

            delegate: Loader {
                id: delegateLoader
                width: listView.width
                sourceComponent: itemDelegateComponent

                property string labelText: text
                property bool busy: ebusy
                property string ipw: eipw
                property ListView view: listView
                property int ourIndex: index

            }
        }
    }




}
