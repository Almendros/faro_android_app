#include "device.h"

#include <qbluetoothaddress.h>
#include <qbluetoothdevicediscoveryagent.h>
#include <qbluetoothlocaldevice.h>
#include <qbluetoothdeviceinfo.h>
#include <qbluetoothservicediscoveryagent.h>
#include <QDebug>
#include <QList>
#include <QMetaEnum>
#include <QTimer>
#include <QSound>

#include <QtAndroidExtras/QAndroidJniObject>


#include "ctes.h"

Device::Device():
    connected(false), controller(0), m_deviceScanState(false), randomAddress(false),
    m_newdevicefound(false)
{
    //! [les-devicediscovery-1]
    discoveryAgent = new QBluetoothDeviceDiscoveryAgent();
    discoveryAgent->setLowEnergyDiscoveryTimeout(5000);
    connect(discoveryAgent, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered,
            this, &Device::addDevice);
    connect(discoveryAgent, QOverload<QBluetoothDeviceDiscoveryAgent::Error>::of(&QBluetoothDeviceDiscoveryAgent::error),
            this, &Device::deviceScanError);
    connect(discoveryAgent, &QBluetoothDeviceDiscoveryAgent::finished, this, &Device::deviceScanFinished);
    //! [les-devicediscovery-1]

    setUpdate("Search");

    timercom = new QTimer(this);
    timercom->setSingleShot(true);
    connect(timercom, SIGNAL(timeout()), this, SLOT(com_serial_timeout()));
    timercom->stop();

    QAndroidJniObject vibroString = QAndroidJniObject::fromString("vibrator");
    QAndroidJniObject activity = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative", "activity", "()Landroid/app/Activity;");
    QAndroidJniObject appctx = activity.callObjectMethod("getApplicationContext","()Landroid/content/Context;");
    vibratorService = appctx.callObjectMethod("getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;", vibroString.object<jstring>());

}
/* ******************************************************************************************** */
/**
 * @brief Genera una vibración en el dispositivo android
 * @param  milliseconds -> tiempo vibración
 * @retval
 */
void Device::vibrate(int milliseconds)
{
    if (vibratorService.isValid()) {
        jlong ms = milliseconds;
        jboolean hasvibro = vibratorService.callMethod<jboolean>("hasVibrator", "()Z");
        vibratorService.callMethod<void>("vibrate", "(J)V", ms);
    }
    else
    {
        qDebug() << "No vibrator service available";
    }
}
/* ******************************************************************************************** */
/**
 * @brief inicialización de timercom
 * @param  timeout -> tiempo de timercom
 * @retval
 */
void Device::com_init_timercom(int timeout)
{
    if (timeout)
    {
        timercom->setInterval(timeout);
        timercom->start();
    }
    else
        timercom->stop();
}
/* ******************************************************************************************** */
/**
 * @brief isr de timercom
 * @param
 * @retval
 */
void Device::com_serial_timeout()
{
    if (TST_FLAG(comEstadoRecepcion, COM_EST_RECEP_WAITING_2_DISCONNECT))
    {
        CLR_FLAG(comEstadoRecepcion, COM_EST_RECEP_WAITING_2_DISCONNECT);
        disconnect2Fan();
    }
}

Device::~Device()
{
    delete discoveryAgent;
    delete controller;
    qDeleteAll(devices);
    qDeleteAll(m_services);
    qDeleteAll(m_characteristics);
    devices.clear();
    m_services.clear();
    m_characteristics.clear();
}
/* ******************************************************************************************** */
/**
 * @brief Comienza la busqueda de dispositivos BLE
 * @param
 * @retval
 */
void Device::startDeviceDiscovery()
{
    comEstadoRecepcion = 0;
    qDeleteAll(devices);
    devices.clear();
    emit devicesUpdated();

    setUpdate("Scanning for devices ...");
    //! [les-devicediscovery-2]
    discoveryAgent->start(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod);
    //! [les-devicediscovery-2]

    if (discoveryAgent->isActive()) {
        m_deviceScanState = true;
        Q_EMIT stateChanged();
    }
}
/* ******************************************************************************************** */
/**
 * @brief Se descubre un nuevo dispositivo BLE, se añade a la lista "devices", dispositivos descubiertos
 * @param
 * @retval
 */
void Device::addDevice(const QBluetoothDeviceInfo &info)
{
    if (info.coreConfigurations() & QBluetoothDeviceInfo::LowEnergyCoreConfiguration) {
        DeviceInfo *d = new DeviceInfo(info);
        devices.append(d);
        setUpdate("Last device added: " + d->getName());
        setNewDevice(true);
    }
}
/* ******************************************************************************************** */
/**
 * @brief Ha terminado la busqueda de los dispositivos BLE que nos rodean
 * @param
 * @retval
 */
void Device::deviceScanFinished()
{
    emit devicesUpdated();
    m_deviceScanState = false;
    emit stateChanged();
    if (devices.isEmpty())
        setUpdate("No Low Energy devices found...");
    else
        setUpdate("Done! Scan Again!");
}
/* ******************************************************************************************** */
/**
 * @brief Devuelve la lista de dispositivos descubiertos
 * @param
 * @retval -> devicesList
 */
QVariant Device::getDevices()
{
    return QVariant::fromValue(devices);
}
/* ******************************************************************************************** */
/**
 * @brief Devuelve la lista de servicios de un dispositivo en concreto
 * @param
 * @retval -> servicesList
 */

QVariant Device::getServices()
{
    return QVariant::fromValue(m_services);
}
/* ******************************************************************************************** */
/**
 * @brief Devuelve la lista de caracteristicas de un dispositivo en concreto
 * @param
 * @retval -> characteristicList
 */
QVariant Device::getCharacteristics()
{
    return QVariant::fromValue(m_characteristics);
}

QString Device::getUpdate()
{
    return m_message;
}
/* ******************************************************************************************** */
/**
 * @brief Se establece conection gatt con el ventilador mac address(scan, inicializacion de signals/slots,..)
 * @param "address" -> mac address ventilador a conectar
 * @retval
 */
void Device::gattConectionGo(const QString &address)
{
    add2connect = address;
    scanServices(add2connect);
}
/* ******************************************************************************************** */
/**
 * @brief Se escanean los servicios gatt con el ventilador mac address
 * @param "address" -> mac address ventilador a conectar
 * @retval
 */
void Device::scanServices(const QString &address)
{
    for (int i = 0; i < devices.size(); i++)
    {
        QVariant v = ((DeviceInfo*)devices.at(i))->getAddress();
        if (((DeviceInfo*)devices.at(i))->getAddress() == address )
            currentDevice.setDevice(((DeviceInfo*)devices.at(i))->getDevice());
    }

    if (!currentDevice.getDevice().isValid()) {
        qWarning() << "Not a valid device";
        return;
    }

    qDeleteAll(m_characteristics);
    m_characteristics.clear();
    emit characteristicsUpdated();
    qDeleteAll(m_services);
    m_services.clear();
    emit servicesUpdated();

    setUpdate("Back\n(Connecting to device...)");


    if (controller && m_previousAddress != currentDevice.getAddress())
    {
        controller->disconnectFromDevice();
        delete controller;
        controller = 0;
    }

    if (!controller)
    {
        // Connecting signals and slots for connecting to LE services.
        controller = new QLowEnergyController(currentDevice.getDevice());
        connect(controller, &QLowEnergyController::connected,
                this, &Device::deviceConnected);
        connect(controller, QOverload<QLowEnergyController::Error>::of(&QLowEnergyController::error),
                this, &Device::errorReceived);
        connect(controller, &QLowEnergyController::disconnected,
                this, &Device::deviceDisconnected);
        connect(controller, &QLowEnergyController::serviceDiscovered,
                this, &Device::addLowEnergyService);
        connect(controller, &QLowEnergyController::discoveryFinished,
                this, &Device::serviceScanDone);
    }

    if (isRandomAddress())
        controller->setRemoteAddressType(QLowEnergyController::RandomAddress);
    else
        controller->setRemoteAddressType(QLowEnergyController::PublicAddress);
   // controller->connectToDevice();
    m_previousAddress = currentDevice.getAddress();

    connect2Fan();

}
/* ******************************************************************************************** */
/**
 * @brief Desconexión gatt
 * @param
 * @retval
 */
void Device::disconnect2Fan(void)
{
    disconnectFromDevice();
    SET_FLAG(comEstadoRecepcion, COM_EST_RECEP_DISCONNECTING_GAT);
}
/* ******************************************************************************************** */
/**
 * @brief Conexión gatt con el ventilador(conexión y escaneo de servicios y permanece conectado)
 * @param
 * @retval
 */
void Device::connect2Fan(void)
{
    controller->connectToDevice();
    SET_FLAG(comEstadoRecepcion, COM_EST_RECEP_CONNECTING_GAT);
}
/* ******************************************************************************************** */
/**
 * @brief Devuelve las rssi de los dispositivos BLE scaneados
 * @param
 * @retval
 */

QList<QVariant> Device::scanRssis(void)
{
    DeviceInfo currentDeviceinfo;
    QList<QVariant> rssi;
    rssi.clear();

    for (int i = 0; i < devices.size(); i++)
    {
        currentDeviceinfo.setDevice(((DeviceInfo*)devices.at(i))->getDevice());
        rssi.append(currentDeviceinfo.getRssi());
    }
    return rssi;
}
/* ******************************************************************************************** */
/**
 * @brief Devuelve los de los dispositivos BLE scaneados
 * @param
 * @retval
 */
QList<QVariant> Device::scanNames(void)
{
    DeviceInfo currentDeviceinfo;
    QList<QVariant> name;
    name.clear();

    for (int i = 0; i < devices.size(); i++)
    {
        currentDeviceinfo.setDevice(((DeviceInfo*)devices.at(i))->getDevice());
        name.append(currentDeviceinfo.getName());
    }
    return name;
}
/* ******************************************************************************************** */
/**
 * @brief
 * @param
 * @retval
 */
void Device::addLowEnergyService(const QBluetoothUuid &serviceUuid)
{
    QLowEnergyService *service = controller->createServiceObject(serviceUuid);
    if (!service) {
        qWarning() << "Cannot create service for uuid";
        return;
    }
    ServiceInfo *serv = new ServiceInfo(service);
    m_services.append(serv);

    emit servicesUpdated();
}
/* ******************************************************************************************** */
/**
 * @brief Fin escaneo de servicios
 * @param
 * @retval
 */
void Device::serviceScanDone()
{
    setUpdate("Back\n(Service scan done!)");
    // force UI in case we didn't find anything
    if (m_services.isEmpty())
        emit servicesUpdated();
}
/* ******************************************************************************************** */
/**
 * @brief Conexión con un servicio uuid para descubrir las características
 * @param
 * @retval
 */

void Device::connectToService(const QString &uuid)
{
    QLowEnergyService *service = 0;
    for (int i = 0; i < m_services.size(); i++)
    {
        ServiceInfo *serviceInfo = (ServiceInfo*)m_services.at(i);
        if (serviceInfo->getUuid() == uuid)
        {
            service = serviceInfo->service();
            break;
        }
    }

    if (!service)
        return;

    qDeleteAll(m_characteristics);
    m_characteristics.clear();
    emit characteristicsUpdated();

    if (service->state() == QLowEnergyService::DiscoveryRequired)
    {
        connect(service, &QLowEnergyService::stateChanged,
                this, &Device::serviceDetailsDiscovered);
        service->discoverDetails();
        setUpdate("Back\n(Discovering details...)");
        return;
    }

    //discovery already done
    const QList<QLowEnergyCharacteristic> chars = service->characteristics();
    foreach (const QLowEnergyCharacteristic &ch, chars) {
        CharacteristicInfo *cInfo = new CharacteristicInfo(ch);
        m_characteristics.append(cInfo);
    }

    QTimer::singleShot(0, this, &Device::characteristicsUpdated);
}
/**
 * @brief slot que se produce cuando se ha establecido una conexión gatt y comienzamos a descubrir los servicios
 * @param
 * @retval
 */
void Device::deviceConnected()
{
    setUpdate("Back\n(Discovering services...)");
    connected = true;
    //! [les-service-2]
    controller->discoverServices();
    //! [les-service-2]
    CLR_FLAG(comEstadoRecepcion, COM_EST_RECEP_CONNECTING_GAT);
    dev_Write_conectionstate(BLE_FAN_CON);
}
/**
 * @brief slot que se produce cuando se ha producido un error en la conexión gatt
 * @param
 * @retval
 */
void Device::errorReceived(QLowEnergyController::Error /*error*/)
{
    qWarning() << "Error: " << controller->errorString();
    setUpdate(QString("Error\n(%1)").arg(controller->errorString()));
    if (TST_FLAG(comEstadoRecepcion, COM_EST_RECEP_CONNECTING_GAT))
    {
        CLR_FLAG(comEstadoRecepcion, COM_EST_RECEP_CONNECTING_GAT);
        dev_Write_conectionstate(BLE_FAN_CON_ERROR);
        com_init_timercom(0);
    }
}
/**
 * @brief
 * @param
 * @retval
 */
void Device::setUpdate(QString message)
{
    m_message = message;
    emit updateChanged();
}
/**
 * @brief desconexión gatt
 * @param
 * @retval
 */
void Device::disconnectFromDevice()
{
     CLR_FLAG(comEstadoRecepcion, COM_EST_RECEP_DISCONNECTING_GAT);
     dev_Write_conectionstate(BLE_FAN_DISCON);

    if (controller->state() != QLowEnergyController::UnconnectedState)
        controller->disconnectFromDevice();
    else
        deviceDisconnected();
}
/**
 * @brief slot que se produce cuando se ha producido por la desconexión gatt
 * @param
 * @retval
 */
void Device::deviceDisconnected()
{
    qWarning() << "Disconnect from device";
    //CLR_FLAG(comEstadoRecepcion, COM_EST_RECEP_ON);
    emit disconnected();
}
/**
 * @brief
 * @param
 * @retval
 */
void Device::serviceDetailsDiscovered(QLowEnergyService::ServiceState newState)
{
    if (newState != QLowEnergyService::ServiceDiscovered) {
        // do not hang in "Scanning for characteristics" mode forever
        // in case the service discovery failed
        // We have to queue the signal up to give UI time to even enter
        // the above mode
        if (newState != QLowEnergyService::DiscoveringServices) {
            QMetaObject::invokeMethod(this, "characteristicsUpdated",
                                      Qt::QueuedConnection);
        }
        return;
    }

    QLowEnergyService *service = qobject_cast<QLowEnergyService *>(sender());
    if (!service)
        return;



    //! [les-chars]
    const QList<QLowEnergyCharacteristic> chars = service->characteristics();
    foreach (const QLowEnergyCharacteristic &ch, chars) {
        CharacteristicInfo *cInfo = new CharacteristicInfo(ch);
        m_characteristics.append(cInfo);
    }
    //! [les-chars]

    emit characteristicsUpdated();
}
/**
 * @brief
 * @param
 * @retval
 */
void Device::deviceScanError(QBluetoothDeviceDiscoveryAgent::Error error)
{
    if (error == QBluetoothDeviceDiscoveryAgent::PoweredOffError)
        setUpdate("The Bluetooth adaptor is powered off, power it on before doing discovery.");
    else if (error == QBluetoothDeviceDiscoveryAgent::InputOutputError)
        setUpdate("Writing or reading from the device resulted in an error.");
    else {
        static QMetaEnum qme = discoveryAgent->metaObject()->enumerator(
                    discoveryAgent->metaObject()->indexOfEnumerator("Error"));
        setUpdate("Error: " + QLatin1String(qme.valueToKey(error)));
    }

    m_deviceScanState = false;
    emit devicesUpdated();
    emit stateChanged();
}
/**
 * @brief
 * @param
 * @retval
 */
bool Device::state()
{
    return m_deviceScanState;
}
/**
 * @brief
 * @param
 * @retval
 */
bool Device::newdevice()
{
    return m_newdevicefound;
}
/**
 * @brief
 * @param
 * @retval
 */
void Device::setNewDevice(bool st)
{
    m_newdevicefound = st;
    emit newdeviceChanged();
}
/**
 * @brief
 * @param
 * @retval
 */
bool Device::hasControllerError() const
{
    if (controller && controller->error() != QLowEnergyController::NoError)
        return true;
    return false;
}
/**
 * @brief
 * @param
 * @retval
 */
bool Device::isRandomAddress() const
{
    return randomAddress;
}
/**
 * @brief
 * @param
 * @retval
 */
void Device::setRandomAddress(bool newValue)
{
    randomAddress = newValue;
    emit randomAddressChanged();
}
/**
 * @brief Escribir dato sobre la caracteristica caracteristicuuid, en serviceuuid y notificada en notifyuuid
 * @param
 * @retval
 */
void Device::writeData(QByteArray data, QString serviceuuid, QString caracteristicuuid, QString notifyuuid)
{
    QByteArray dataSending;
    QLowEnergyService *service = 0;
    for (int i = 0; i < m_services.size(); i++)
    {
        ServiceInfo *serviceInfo = (ServiceInfo*)m_services.at(i);
        if (serviceInfo->getUuid() == serviceuuid)
        {
            service = serviceInfo->service();
            break;
        }
    }
    QLowEnergyCharacteristic characteristic, characteristicnotif;
    for (int i = 0; i < m_characteristics.size(); i++)
    {
        CharacteristicInfo *characteristicinfo = (CharacteristicInfo*)m_characteristics.at(i);
        if (characteristicinfo->getUuid() == caracteristicuuid)
        {
            characteristic = characteristicinfo->getCharacteristic();
        }
        else if (characteristicinfo->getUuid() == notifyuuid)
        {
            characteristicnotif = characteristicinfo->getCharacteristic();
        }
    }
    QLowEnergyDescriptor notification = characteristicnotif.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
    connect(service, SIGNAL(characteristicChanged(QLowEnergyCharacteristic,QByteArray)),
            this, SLOT(characteristicChanged(QLowEnergyCharacteristic,  QByteArray)));

    // enable notification
    service->writeDescriptor(notification, QByteArray::fromHex("0100"));

    service->writeCharacteristic(characteristic, data, QLowEnergyService::WriteWithResponse);
}
/**
 * @brief Convertir unsigned int en QByteArray
 * @param val -> valor a convertir
 * @retval
 */

QByteArray Device::sendint2char(unsigned int val)
{
    QByteArray ba; ba.clear();
    ba[0] = val & 0x000000FF; ba[1] = (val & 0x0000FF00) >> 8; ba[2] = (val & 0x00FF0000) >> 16; ba[3] = (val & 0xFF000000) >> 24;
    return ba;
}
/* ******************************************************************************************** */
/**
 * @brief Previamente hemos hecho el escaneo, o sea, conocemos los servicios y las caracteristicas, comparamos
 *        que se trata del servicio/caracteristica que toca y enviamos tecla
 * @param
 * @retval
 */
void Device::sendTeclaRf(unsigned int tecla, QString serviceuuid, QString caracteristicuuid, QString notifyuuid)
{
    QLowEnergyService *service = 0;
    for (int i = 0; i < m_services.size(); i++)
    {
        ServiceInfo *serviceInfo = (ServiceInfo*)m_services.at(i);
        if (serviceInfo->getUuid() == serviceuuid)
        {
            service = serviceInfo->service();
            break;
        }
    }
    QLowEnergyCharacteristic characteristic, characteristicnotif;
    for (int i = 0; i < m_characteristics.size(); i++)
    {
        CharacteristicInfo *characteristicinfo = (CharacteristicInfo*)m_characteristics.at(i);
        if (characteristicinfo->getUuid() == caracteristicuuid)
        {
            characteristic = characteristicinfo->getCharacteristic();
        }
        else if (characteristicinfo->getUuid() == notifyuuid)
        {
            characteristicnotif = characteristicinfo->getCharacteristic();
        }
    }
    QLowEnergyDescriptor notification = characteristicnotif.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
    connect(service, SIGNAL(characteristicChanged(QLowEnergyCharacteristic,QByteArray)),
            this, SLOT(characteristicChanged(QLowEnergyCharacteristic,  QByteArray)));

    QByteArray data1; data1.clear();
    data1.append(COM_BLE_FRAME_INI);
    data1.append(RF_vent_2_send.commBle);
    data1.append(RF_vent_2_send.protocolo);
    data1.append(RF_vent_2_send.nBits);
    data1.append(RF_vent_2_send.reservado);
    data1.append(sendint2char(RF_vent_2_send.mandoDirecc));
    RF_vent_2_send.commRf = RF_cmd_tecla[tecla];
    data1.append(sendint2char(RF_vent_2_send.commRf));
   // RF_vent_2_send.chk = 0;
   // data1.append(sendint2char(RF_vent_2_send.chk));
   // data1.append(sendint2char(0x12345678));
    data1.append(COM_BLE_FRAME_FIN);

    service->writeCharacteristic(characteristic, data1, QLowEnergyService::WriteWithoutResponse);
    //com_init_timercom(100);
   // SET_FLAG(comEstadoRecepcion, COM_EST_RECEP_WAITING_2_DISCONNECT);
}
/**
 * @brief
 * @param
 * @retval
 */
void Device::characteristicChanged(QLowEnergyCharacteristic,  QByteArray d)
{
    qDebug() << d;
}
/* ******************************************************************************************** */
/**
 * @brief Write "conectionstate"
 * @param conectionstate -> BLE_FAN_DISCON = 0, BLE_FAN_CON  = 1, BLE_FAN_CON_ERROR = 2
 * @retval
 */
void Device::dev_Write_conectionstate(const int &h)
{
    m_conectionstate = h;
    emit dev_Changed_conectionstate();
}
/* ******************************************************************************************** */
/**
 * @brief Read "conectionstate"
 * @param conectionstate -> BLE_FAN_DISCON = 0, BLE_FAN_CON  = 1, BLE_FAN_CON_ERROR = 2
 * @retval
 */
int Device::dev_Read_conectionstate(void)
{
    return m_conectionstate;
}
/**
 * @brief Inicialización parámetros correspondientes la mando tipo 1
 * @param
 * @retval
 */
void Device::ini_Ventilador_tipo_1(void)
{
    RF_vent_2_send.nBits = 32; RF_vent_2_send.protocolo = BLE_RF_PROTOCOL_2;
    RF_vent_2_send.mandoDirecc = BLE_MANDO_DIRECC_AZUL; RF_vent_2_send.commBle = BLE_SEN_CMD_RF_COM;

    RF_cmd_tecla[BLE_MANDO_KEY_1] = BLE_VENT_TIPO1_KEY1; RF_cmd_tecla[BLE_MANDO_KEY_2] = BLE_VENT_TIPO1_KEY2;
    RF_cmd_tecla[BLE_MANDO_KEY_3] = BLE_VENT_TIPO1_KEY3; RF_cmd_tecla[BLE_MANDO_KEY_4] = BLE_VENT_TIPO1_KEY4;
    RF_cmd_tecla[BLE_MANDO_KEY_5] = BLE_VENT_TIPO1_KEY5; RF_cmd_tecla[BLE_MANDO_KEY_6] = BLE_VENT_TIPO1_KEY6;
    RF_cmd_tecla[BLE_MANDO_KEY_POWER] = BLE_VENT_TIPO1_POWER; RF_cmd_tecla[BLE_MANDO_KEY_LIGHT_ON] = BLE_VENT_TIPO1_LUZ_ON;
    RF_cmd_tecla[BLE_MANDO_KEY_LIGHT_OFF] = BLE_VENT_TIPO1_LUZ_OFF;
    RF_cmd_tecla[BLE_MANDO_KEY_FLAP] = BLE_VENT_TIPO1_FLAP; RF_cmd_tecla[BLE_MANDO_KEY_FAN] = BLE_VENT_TIPO1_FAN;
    RF_cmd_tecla[BLE_MANDO_KEY_TEMP] = BLE_VENT_TIPO1_TEMP; RF_cmd_tecla[BLE_MANDO_KEY_1H] = BLE_VENT_TIPO1_1H;
    RF_cmd_tecla[BLE_MANDO_KEY_4H] = BLE_VENT_TIPO1_4H; RF_cmd_tecla[BLE_MANDO_KEY_8H] = BLE_VENT_TIPO1_8H;

}
/* ******************************************************************************************** */
/**
 * @brief Inicializamos la list de dispositivos visibles "device"(le decimos al stack el dispositivo que está visible, su mac, su nombre,
 * y el tipo de mando a distancia)
 * @param
 * @retval
 */
void Device::startProcessBle()
{
    comEstadoRecepcion = 0;
    qDeleteAll(devices);
    devices.clear();
    emit devicesUpdated();
    m_deviceScanState = false;
    Q_EMIT stateChanged();
    dev_Write_conectionstate(0);

    //Inicializacion de nuestras condiciones actuales
    QBluetoothAddress address;
    address = QBluetoothAddress("30:AE:A4:15:17:2A");
    //address = QBluetoothAddress("24:6F:28:48:B6:92");

    QBluetoothDeviceInfo info;
    //info = QBluetoothDeviceInfo(address, "snd48",0);
    info = QBluetoothDeviceInfo(address, "ESP32",0);
    DeviceInfo *d = new DeviceInfo(info);
    devices.append(d);
    setUpdate("Last device added: " + d->getName());
    setNewDevice(true);

    ini_Ventilador_tipo_1();

}
