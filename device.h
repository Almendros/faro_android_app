/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the demonstration applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DEVICE_H
#define DEVICE_H

#include <qbluetoothlocaldevice.h>
#include <QObject>
#include <QVariant>
#include <QList>
#include <QBluetoothServiceDiscoveryAgent>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QLowEnergyController>
#include <QBluetoothServiceInfo>
#include "deviceinfo.h"
#include "serviceinfo.h"
#include "characteristicinfo.h"
#include "ble_defs.h"
#include <QTimer>
#include <QAndroidJniEnvironment>
#include <QAndroidJniObject>

QT_FORWARD_DECLARE_CLASS (QBluetoothDeviceInfo)
QT_FORWARD_DECLARE_CLASS (QBluetoothServiceInfo)

class Device: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariant devicesList READ getDevices NOTIFY devicesUpdated)
    Q_PROPERTY(QVariant servicesList READ getServices NOTIFY servicesUpdated)
    Q_PROPERTY(QVariant characteristicList READ getCharacteristics NOTIFY characteristicsUpdated)
    Q_PROPERTY(QString update READ getUpdate WRITE setUpdate NOTIFY updateChanged)
    Q_PROPERTY(bool useRandomAddress READ isRandomAddress WRITE setRandomAddress NOTIFY randomAddressChanged)
    Q_PROPERTY(bool state READ state NOTIFY stateChanged)
    Q_PROPERTY(bool controllerError READ hasControllerError)
    Q_PROPERTY(bool newdevice READ newdevice WRITE setNewDevice NOTIFY newdeviceChanged)
    Q_PROPERTY(int conectionstate READ dev_Read_conectionstate WRITE dev_Write_conectionstate NOTIFY dev_Changed_conectionstate)
public:
    Device();
    ~Device();
    QTimer *timercom;//, *timerdlys;
    QVariant getDevices();
    QVariant getServices();
    QVariant getCharacteristics();
    QString getUpdate();
    bool state();
    bool newdevice();
    bool hasControllerError() const;

    bool isRandomAddress() const;
    void setRandomAddress(bool newValue);



    Q_INVOKABLE void setNewDevice(bool st);

    Q_INVOKABLE void writeData(QByteArray data, QString serviceuuid, QString caracteristicuuid, QString notifyuuid);
    Q_INVOKABLE QList<QVariant> scanRssis(void);
    Q_INVOKABLE QList<QVariant> scanNames(void);

    Q_INVOKABLE void vibrate(int milliseconds);
    Q_INVOKABLE void gattConectionGo(const QString &address);


    Q_INVOKABLE void disconnect2Fan(void);
    Q_INVOKABLE void connect2Fan(void);
    Q_INVOKABLE void startProcessBle(void);


    Q_INVOKABLE void sendTeclaRf(unsigned int tecla, QString serviceuuid, QString caracteristicuuid, QString notifyuuid);



public slots:
    void startDeviceDiscovery();
    void scanServices(const QString &address);

    void connectToService(const QString &uuid);
    void disconnectFromDevice();
    void com_serial_timeout();
    //void dlys_timer();

private slots:
    // QBluetoothDeviceDiscoveryAgent related
    void addDevice(const QBluetoothDeviceInfo&);
    void deviceScanFinished();
    void deviceScanError(QBluetoothDeviceDiscoveryAgent::Error);

    // QLowEnergyController realted
    void addLowEnergyService(const QBluetoothUuid &uuid);
    void deviceConnected();
    void errorReceived(QLowEnergyController::Error);
    void serviceScanDone();
    void deviceDisconnected();

    // QLowEnergyService related
    void serviceDetailsDiscovered(QLowEnergyService::ServiceState newState);

    void characteristicChanged(QLowEnergyCharacteristic,  QByteArray);

Q_SIGNALS:
    void devicesUpdated();
    void servicesUpdated();
    void characteristicsUpdated();
    void updateChanged();
    void stateChanged();
    void disconnected();
    void randomAddressChanged();
    void newdeviceChanged();
    void dev_Changed_conectionstate();

private:
    void setUpdate(QString message);
    //void setNewDevice(bool st);
    QBluetoothDeviceDiscoveryAgent *discoveryAgent;
    DeviceInfo currentDevice;
    QList<QObject*> devices;
    QList<QObject*> m_services;
    QList<QObject*> m_characteristics;
    //QList<qint16> m_rssi;
    QString m_previousAddress;
    QString m_message;
    bool connected;
    QLowEnergyController *controller;
    bool m_deviceScanState;
    bool randomAddress;
    bool m_newdevicefound;
    int m_devavisstate;
    QString m_serviceuuid, m_caracteristicuuid, m_notifyuuid;
    unsigned int comEstadoRecepcion = 0;
    unsigned int StatedlysTimer = 0;
    //rec_frame_struct_t com_rec_frame_st;
    void com_init_timercom(int timeout);
    void dev_Write_conectionstate(const int &h);
    int dev_Read_conectionstate(void);
     QAndroidJniObject vibratorService;
     QString add2connect;
     int m_mesacepcruzar = 0;
     int dev_Read_mesacepcruzar(void);
     int m_conectionstate = 0;
     unsigned int  RF_cmd_tecla[16];
     //RF_cmd_tecla_type RF_cmd_tecla[1];
     RF_send_struct_type RF_vent_2_send;
     void ini_Ventilador_tipo_1(void);
     QByteArray sendint2char(unsigned int);




     //int gattconnectiter;
};

#endif // DEVICE_H
