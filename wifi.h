#ifndef WIFI_H
#define WIFI_H

#include <QDebug>
#include <QByteArray>
#include <QTimer>


class Wifi : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool wfconnectedst WRITE wifi_Write_wfconnectedst  READ wifi_Read_wfconnectedst NOTIFY wifi_Changed_wfconnectedst)
    Q_PROPERTY(int wfownccid WRITE wifi_Write_wfownccid  READ wifi_Read_wfownccid NOTIFY wifi_Changed_wfownccid)
    Q_PROPERTY(QString wfownccidstr WRITE wifi_Write_wfownccidstr  READ wifi_Read_wfownccidstr NOTIFY wifi_Changed_wfownccidstr)
    Q_PROPERTY(int wfnreadccids WRITE wifi_Write_wfnreadccids  READ wifi_Read_wfnreadccids NOTIFY wifi_Changed_wfnreadccids)
    Q_PROPERTY(QString wfnreadccidstr WRITE wifi_Write_wfnreadccidstr  READ wifi_Read_wfnreadccidstr NOTIFY wifi_Changed_wfnreadccidstr)
    Q_PROPERTY(int wfindexccid WRITE wifi_Write_wfindexccid  READ wifi_Read_wfindexccid NOTIFY wifi_Changed_wfindexccid)
    Q_PROPERTY(QString wfownipstr WRITE wifi_Write_wfownipstr  READ wifi_Read_wfownipstr NOTIFY wifi_Changed_wfownipstr)

    Q_PROPERTY(int esp32wifiid WRITE wifi_Write_esp32wifiid  READ wifi_Read_esp32wifiid NOTIFY wifi_Changed_esp32wifiid)
public:
    Wifi();
    QTimer *timercom, *timercheckconn;
    Q_INVOKABLE void wifi_Write_wfownccid(const int &h);
    Q_INVOKABLE int wifi_Read_wfownccid(void);
    Q_INVOKABLE void wifi_Write_wfownccidstr(const QString &h);
    Q_INVOKABLE QString wifi_Read_wfownccidstr(void);

    Q_INVOKABLE void wifi_Write_wfnreadccids(const int &h);
    Q_INVOKABLE int wifi_Read_wfnreadccids(void);

    Q_INVOKABLE void wifi_Write_wfnreadccidstr(const QString &h);
    Q_INVOKABLE QString wifi_Read_wfnreadccidstr(void);

    Q_INVOKABLE void wifi_Write_wfindexccid(const int &h);
    Q_INVOKABLE int wifi_Read_wfindexccid(void);

    Q_INVOKABLE void wifi_Connect_AP(const int &h);

    Q_INVOKABLE void wifi_Write_wfconnectedst(const bool &h);
    Q_INVOKABLE bool wifi_Read_wfconnectedst(void);

    Q_INVOKABLE void wifi_Write_esp32wifiid(const int &h);
    Q_INVOKABLE int wifi_Read_esp32wifiid(void);

    Q_INVOKABLE void wifi_Write_wfownipstr(const QString &h);
    Q_INVOKABLE QString wifi_Read_wfownipstr(void);

private:
    int wfownccid, wfnreadccids, wfindexccid, wfconndelay;
    int esp32wifiid = 0;
    bool wfconnectedst = false;
    QString wfownccidstr, wfnreadccidstr, wfownipstr;
    QList<QString> ssidread;
    QString wifi_CCID_Format(QString cad);
    void wifi_dly(int dly);
signals:
    void wifi_Changed_wfownccid();
    void wifi_Changed_wfownccidstr();
    void wifi_Changed_wfnreadccids();
    void wifi_Changed_wfnreadccidstr();
    void wifi_Changed_wfindexccid();
    void wifi_Changed_wfconnectedst();
    void wifi_Changed_esp32wifiid();
    void wifi_Changed_wfownipstr();
public slots:
    void com_serial_timeout();
    void com_check_connect();

};

#endif // WIFI_H
