#ifndef CTES_H
#define CTES_H

#define SET_FLAG(ADDRESS,MASK) ((ADDRESS) |= (MASK))
#define CLR_FLAG(ADDRESS,MASK) ((ADDRESS) &= static_cast<unsigned int>(~(MASK)))
#define TGL_FLAG(ADDRESS,MASK) ((ADDRESS) ^= MASK)
#define TST_FLAG(ADDRESS,MASK) ((ADDRESS) & MASK)

#define ABS(X) ((X) > 0 ? (X) : -(X))
#define MAX_UINT 0xFFFFFFFF
#define uint unsigned int

#endif // CTES_H
