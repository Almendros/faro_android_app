
import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Universal 2.1
import Qt.labs.settings 1.0
//import com.qt.Wifi 1.0


ApplicationWindow {
    id: approot
    width: 360
    height: 520
    visible: true
    title: "Lucid"
    property var itemstack
    property alias stackView1: stackView1
//    Wifi
//    {
//        id: objwifi
//        onWfconnectedstChanged:
//        {
//            console.log(esp32wifiid-1);
//            itemstack.petsModel.set(esp32wifiid-1, {"ebusy": !wfconnectedst, "eipw": wfownipstr});
//        }
//    }
    function onjumpfrombuscarredes(msg)
    {
        console.log(msg);
       // objwifi.wifi_Connect_AP(msg);
    }

    Settings {
        id: settings
        property string style: "Default"
    }

    Shortcut {
        sequences: ["Esc", "Back"]
        enabled: stackView1.depth > 1
        onActivated: {
            stackView1.pop()
            listView.currentIndex = -1
        }
    }

    Shortcut {
        sequence: "Menu"
        onActivated: optionsMenu.open()
    }

    header: ToolBar {
        Material.foreground: "white"

        RowLayout {
            spacing: 20
            anchors.fill: parent

            ToolButton {
                //icon.name: stackView.depth > 1 ? "back" : "drawer"
                icon.source: stackView1.depth > 1 ? "assets/back.png" : "assets/drawer.png"
                onClicked: {
                    if (stackView1.depth > 1) {
                        stackView1.pop()
                        listView.currentIndex = -1
                    } else {
                        drawer.open()
                    }
                }
            }

            Label {
                id: titleLabel
                text: listView.currentItem ? listView.currentItem.text : "Faro"
                font.pixelSize: 20
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }

            ToolButton {
                icon.name: "menu"
                onClicked: optionsMenu.open()

                Menu {
                    id: optionsMenu
                    x: parent.width - width
                    transformOrigin: Menu.TopRight

                    MenuItem {
                        text: "Settings"
                        onTriggered: settingsDialog.open()
                    }
                    MenuItem {
                        text: "About"
                        onTriggered: aboutDialog.open()
                    }
                }
            }
        }
    }

    Drawer {
        id: drawer
        width: Math.min(approot.width, approot.height) / 3 * 2
        height: approot.height
        interactive: stackView1.depth === 1
        Image {
            id: logodanfoss
            width: 800
           // source: "assets/faro.png"
            fillMode: Image.PreserveAspectFit
        }
        ListView {
            id: listView
            anchors.topMargin: 60

            focus: true
            currentIndex: -1
            anchors.fill: parent



            delegate: ItemDelegate {
                width: parent.width
                text: model.title
                font.pixelSize: 14
                highlighted: ListView.isCurrentItem
                onClicked: {
                    listView.currentIndex = index
                    itemstack = stackView1.push(model.source);
//                    if (index === 0)
//                    {
//                        itemstack.busywifigeneral.running = true
//                        itemstack.jumpfrombuscarredes.connect(onjumpfrombuscarredes);
//                        var nredes = objwifi.wifi_Read_wfnreadccids();
//                        if (nredes)
//                        {
//                            for (var j = 0; j < nredes; j++)
//                            {
//                                objwifi.wifi_Write_wfindexccid(j);
//                                var ccid = objwifi.wifi_Read_wfnreadccidstr();
//                                itemstack.petsModel.insert(j, {"text": ccid, "ebusy": false, "eipw": "0.0.0.0"});
//                            }
//                        }
//                        itemstack.busywifigeneral.running = false
//                    }
//                    else if (indedx  === 1)
//                    {
//                        itemstack.jumpfrombuscarredes.connect(onjumpfrombuscarredes);

//                    }

                    drawer.close()
                }
            }

            model: ListModel {
                  //ListElement { title: "WiFis APs Visibles"; source: "BuscarRedes.qml" }
                  //ListElement { title: "Radiadores BLE visibles"; source: "PageInvidente.qml" }
                  ListElement { title: "Buscar"; source: "BleScanPage.qml" }
                  ListElement { title: "Faro Remote Control"; source: "PageRemoteControl.qml" }
//                ListElement { title: "CheckBox"; source: "qrc:/pages/CheckBoxPage.qml" }
//                ListElement { title: "ComboBox"; source: "qrc:/pages/ComboBoxPage.qml" }
//                ListElement { title: "DelayButton"; source: "qrc:/pages/DelayButtonPage.qml" }
//                ListElement { title: "Dial"; source: "qrc:/pages/DialPage.qml" }
//                ListElement { title: "Dialog"; source: "qrc:/pages/DialogPage.qml" }
//                ListElement { title: "Delegates"; source: "qrc:/pages/DelegatePage.qml" }
//                ListElement { title: "Frame"; source: "qrc:/pages/FramePage.qml" }
//                ListElement { title: "GroupBox"; source: "qrc:/pages/GroupBoxPage.qml" }
//                ListElement { title: "PageIndicator"; source: "qrc:/pages/PageIndicatorPage.qml" }
//                ListElement { title: "ProgressBar"; source: "qrc:/pages/ProgressBarPage.qml" }
//                ListElement { title: "RadioButton"; source: "qrc:/pages/RadioButtonPage.qml" }
//                ListElement { title: "RangeSlider"; source: "qrc:/pages/RangeSliderPage.qml" }
//                ListElement { title: "ScrollBar"; source: "qrc:/pages/ScrollBarPage.qml" }
//                ListElement { title: "ScrollIndicator"; source: "qrc:/pages/ScrollIndicatorPage.qml" }
//                ListElement { title: "Slider"; source: "qrc:/pages/SliderPage.qml" }
//                ListElement { title: "SpinBox"; source: "qrc:/pages/SpinBoxPage.qml" }
//                ListElement { title: "StackView"; source: "qrc:/pages/StackViewPage.qml" }
//                ListElement { title: "SwipeView"; source: "qrc:/pages/SwipeViewPage.qml" }
//                ListElement { title: "Switch"; source: "qrc:/pages/SwitchPage.qml" }
//                ListElement { title: "TabBar"; source: "qrc:/pages/TabBarPage.qml" }
//                ListElement { title: "TextArea"; source: "qrc:/pages/TextAreaPage.qml" }
//                ListElement { title: "TextField"; source: "qrc:/pages/TextFieldPage.qml" }
//                ListElement { title: "ToolTip"; source: "qrc:/pages/ToolTipPage.qml" }
//                ListElement { title: "Tumbler"; source: "qrc:/pages/TumblerPage.qml" }
            }

            ScrollIndicator.vertical: ScrollIndicator { }
        }
    }

    StackView {
        id: stackView1
        anchors.fill: parent

        initialItem: Pane {
            id: pane

            Image {
                id: logo
                width: pane.availableWidth / 2
                height: pane.availableHeight / 2
                anchors.centerIn: parent
                anchors.verticalCenterOffset: -50
                fillMode: Image.PreserveAspectFit
                source: "assets/faro.png"
            }
//            Button {
//                id: button
//                text: qsTr("Button")
//                anchors.horizontalCenter: parent.horizontalCenter
//                anchors.top: parent.top
//                anchors.topMargin: 300
//                onClicked:
//                {
//                    //objwifi.wifi_Read_wfownccidstr();
//                   // objwifi.wifi_Write_wfownccid(1);
//                    //idbuscarredes.objwifi.wifi_Write_wfownccid("pepe");
//                    //objwifi.wifi_Write_wfownccid("hello");
//    //                petsModel.append({ "text": "ItemDelegate123", ebusy: true})
//                    //petsModel.append({ busy: true})
//                }
//            }



//            Label {
//                text: "Qt Quick Controls 2 provides a set of controls that can be used to build complete interfaces in Qt Quick."
//                anchors.margins: 20
//                anchors.top: logo.bottom
//                anchors.left: parent.left
//                anchors.right: parent.right
//                anchors.bottom: arrow.top
//                horizontalAlignment: Label.AlignHCenter
//                verticalAlignment: Label.AlignVCenter
//                wrapMode: Label.Wrap
//            }

            Image {
                id: arrow
                source: "assets/arrow.png"
                anchors.left: parent.left
                anchors.bottom: parent.bottom
            }
        }
    }

    Dialog {
        id: settingsDialog
        x: Math.round((approot.width - width) / 2)
        y: Math.round(approot.height / 6)
        width: Math.round(Math.min(approot.width, approot.height) / 3 * 2)
        modal: true
        focus: true
        title: "Settings"

        standardButtons: Dialog.Ok | Dialog.Cancel
        onAccepted: {
            settings.style = styleBox.displayText
            settingsDialog.close()
        }
        onRejected: {
            styleBox.currentIndex = styleBox.styleIndex
            settingsDialog.close()
        }

        contentItem: ColumnLayout {
            id: settingsColumn
            spacing: 20

            RowLayout {
                spacing: 10

                Label {
                    text: "Style:"
                }

//                ComboBox {
//                    id: styleBox
//                    property int styleIndex: -1
//                    //model: availableStyles
//                    Component.onCompleted: {
//                        styleIndex = find(settings.style, Qt.MatchFixedString)
//                        if (styleIndex !== -1)
//                            currentIndex = styleIndex
//                    }
//                    Layout.fillWidth: true
//                }
            }

//            Label {
//                text: "Restart required"
//                color: "#e41e25"
//                opacity: styleBox.currentIndex !== styleBox.styleIndex ? 1.0 : 0.0
//                horizontalAlignment: Label.AlignHCenter
//                verticalAlignment: Label.AlignVCenter
//                Layout.fillWidth: true
//                Layout.fillHeight: true
//            }
        }
    }

    Dialog {
        id: aboutDialog
        modal: true
        focus: true
        title: "About"
        x: (approot.width - width) / 2
        y: approot.height / 6
        width: Math.min(approot.width, approot.height) / 3 * 2
        contentHeight: aboutColumn.height

        Column {
            id: aboutColumn
            spacing: 20

            Label {
                width: aboutDialog.availableWidth
                text: "The Qt Quick Controls 2 module delivers the next generation user interface controls based on Qt Quick."
                wrapMode: Label.Wrap
                font.pixelSize: 12
            }

            Label {
                width: aboutDialog.availableWidth
                text: "In comparison to the desktop-oriented Qt Quick Controls 1, Qt Quick Controls 2 "
                    + "are an order of magnitude simpler, lighter and faster, and are primarily targeted "
                    + "towards embedded and mobile platforms."
                wrapMode: Label.Wrap
                font.pixelSize: 12
            }
        }
    }
}
