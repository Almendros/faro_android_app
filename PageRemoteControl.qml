import QtQuick 2.6
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.1
import QtQuick.Controls.Styles 1.4

Page
{
    id: page
    property int fanconState: device.conectionstate
    property var servicesList : device.servicesList
    property var characteristicList : device.characteristicList
    property string nservicio
    property string ncarac
    property string nnotify

    property int tecla: 0
    property int intentos: 0
    property bool connecting: false
    onFanconStateChanged:
    {
        if (fanconState === 2)
        {
            intentos++;
            if (intentos < 5)
            {
                device.disconnect2Fan();
                device.gattConectionGo("30:AE:A4:15:17:2A");
            }
        }
    }
    onServicesListChanged:
    {
        var list = device.servicesList
        var keysArray = Object.keys(list);
        var longitud = keysArray.length;
        for (var j = 0; j < longitud; j++)
        {
            var str = j.toString();
            var obj = list[str];
            for (var prop in obj)
            {
                console.log(prop + ":" + obj[prop]);
                if (prop === "serviceUuid" && obj[prop] === "4880c12c-fdcb-4077-8920-a450d7f9b907")
                {
                    console.log("Tiene el service UUID que esperamos");
                    device.connectToService(obj[prop]);
                    nservicio = obj[prop];
                    break;
                }
            }
        }
    }
    onCharacteristicListChanged:
    {
        var list = device.characteristicList
        var keysArray = Object.keys(list);
        var longitud = keysArray.length;
        var servicerdy = 0;
        ncarac = "";
        nnotify = "";

        for (var j = 0; j < longitud; j++)
        {
            var str = j.toString();
            var obj = list[str];
            for (var prop in obj)
            {
                console.log(prop + ":" + obj[prop]);
                if (prop === "characteristicUuid" && obj[prop] === "fec26ec4-6d71-4442-9f81-55bc21d658d6")
                {
                    console.log("Tiene el characteristics UUID que esperamos");
                    nnotify = obj[prop]; ncarac = obj[prop];
                    servicerdy += 1;
                }

            }
        }
        if (servicerdy)
        {
            connecting = false;
            if ((tecla == 6) && (fanconState == 1))
                device.sendTeclaRf(tecla, nservicio, ncarac, nnotify);
       }
    }
    Rectangle
    {
        id: rectpower
        y: 39
        width: 60
        height: 60
        color: "#ebf0f0"
        radius: width*0.5
        anchors.left: parent.left
        anchors.leftMargin: 20
        //border.width: 1
        Image
        {
            id: imgpower
            x: 41
            y: 34
            width: 30
            height: 30
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            source: "assets/power_red.png"
        }
        MouseArea
        {
            anchors.fill: parent
            onPressed:
            {
                if (connecting === false)
                {
                    imgpower.source =  "assets/power_white.png"
                    if (fanconState == 0)
                        device.gattConectionGo("30:AE:A4:15:17:2A");
                    else if (fanconState == 1)
                        device.disconnect2Fan();
                    tecla = 6;
                    intentos = 0;
                    connecting = true;
                }
            }
            onReleased:
            {
                imgpower.source = "assets/power_red.png"
            }
        }
    }
    Rectangle
    {
        id: rectlighton
        width: 60
        height: 60
        color: "#ebf0f0"
        radius: width*0.5
        anchors.top: parent.top
        anchors.topMargin: 300
        anchors.left: parent.left
        anchors.leftMargin: 86
        //border.width: 1
        Image
        {
            id: imglighton
            x: 41
            y: 34
            width: 50
            height: 50
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            source: "assets/light_on_black.png"
        }
        MouseArea
        {
            anchors.fill: parent
            onPressed:
            {
                imglighton.source =  "assets/light_on_white.png"
                //device.gattConectionGo("30:AE:A4:15:17:2A");
                tecla = 7; intentos = 0;
                if (fanconState == 1)
                    device.sendTeclaRf(tecla, nservicio, ncarac, nnotify);
            }
            onReleased:
            {
                imglighton.source = "assets/light_on_black.png"
            }
            
        }
    }
    Rectangle
    {
        id: rectlightoff
        width: 60
        height: 60
        color: "#ebf0f0"
        radius: width*0.5
        anchors.right: parent.right
        anchors.rightMargin: 86
        anchors.top: parent.top
        anchors.topMargin: 300
        //border.width: 1
        Image
        {
            id: imglightoff
            x: 41
            y: 34
            width: 50
            height: 50
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            source: "assets/light_off_black.png"
        }
        MouseArea
        {
            anchors.fill: parent
            onPressed:
            {
                imglightoff.source =  "assets/light_off_white.png"
               // device.gattConectionGo("30:AE:A4:15:17:2A");
                tecla = 8; intentos = 0;
                if (fanconState == 1)
                    device.sendTeclaRf(tecla, nservicio, ncarac, nnotify);
            }
            onReleased:
            {
                imglightoff.source = "assets/light_off_black.png"
            }
            
        }
    }
    Rectangle
    {
        id: rectloop
        width: 60
        height: 60
        color: "#ebf0f0"
        radius: width*0.5
        anchors.top: parent.top
        anchors.topMargin: 400
        anchors.left: parent.left
        anchors.leftMargin: 86
        //border.width: 1
        Image
        {
            id: imgloop
            x: 41
            y: 34
            width: 40
            height: 40
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            source: "assets/loop_black.png"
        }
        MouseArea
        {
            anchors.fill: parent
            onPressed:
            {
                imgloop.source =  "assets/loop_white.png"
               // device.gattConectionGo("30:AE:A4:15:17:2A");
                tecla = 9; intentos = 0;
                if (fanconState == 1)
                    device.sendTeclaRf(tecla, nservicio, ncarac, nnotify);
            }
            onReleased:
            {
                imgloop.source = "assets/loop_black.png"
            }
            
        }
    }
    Rectangle
    {
        id: recttemperature
        width: 60
        height: 60
        color: "#ebf0f0"
        radius: width*0.5
        anchors.top: parent.top
        anchors.topMargin: 400
        anchors.right: parent.right
        anchors.rightMargin: 86
        //border.width: 1
        Image
        {
            id: imgtemperature
            x: 41
            y: 34
            width: 30
            height: 40
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            source: "assets/temperature_black.png"
        }
        MouseArea
        {
            anchors.fill: parent
            onPressed:
            {
                imgtemperature.source =  "assets/temperature_white.png"
                //device.gattConectionGo("30:AE:A4:15:17:2A");
                tecla = 11; intentos = 0;
                if (fanconState == 1)
                    device.sendTeclaRf(tecla, nservicio, ncarac, nnotify);
            }
            onReleased:
            {
                imgtemperature.source = "assets/temperature_black.png"
            }
            
        }
    }
    
    
    Rectangle
    {
        id: rectangle
        x: 87
        y: 115
        width: 158
        height: 158
        color: "#ebf0f0"
        radius: width*0.5
        anchors.horizontalCenter: parent.horizontalCenter
        
        Rectangle {
            x: 0
            y: 0
            width: 70
            height: 70
            color: "#ebf0f0"
            radius: width*0.5
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenterOffset: 0
            border.width: 2
            border.color: "#0a2f4a"
            anchors.horizontalCenter: parent.horizontalCenter
            
            Image
            {
                id: imgfancentral
                x: 20
                y: 21
                width: 28
                height: 26
                source: "assets/fancentral_black.png"
                MouseArea
                {
                    anchors.fill: parent
                    onPressed:
                    {
                        imgfancentral.source =  "assets/fancentral_white.png"
                       // device.gattConectionGo("30:AE:A4:15:17:2A");
                        tecla = 10; intentos = 0;
                        if (fanconState == 1)
                            device.sendTeclaRf(tecla, nservicio, ncarac, nnotify);
                    }
                    onReleased:
                    {
                        imgfancentral.source =  "assets/fancentral_black.png"
                    }
                    
                }
            }
        }
        
        Label
        {
            id: lbltres
            x: 37
            y: 16
            width: 27
            height: 29
            color: "black"
            text: qsTr("3")
            anchors.verticalCenterOffset: -50
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenterOffset: -30
            anchors.horizontalCenter: parent.horizontalCenter
            font.italic: false
            font.bold: true
            font.pointSize: 17
            horizontalAlignment: Text.AlignHCenter
            MouseArea
            {
                anchors.fill: parent
                onPressed:
                {
                    lbltres.color = "white"
                    //device.gattConectionGo("30:AE:A4:15:17:2A");
                    tecla = 2; intentos = 0;
                    if (fanconState == 1)
                        device.sendTeclaRf(tecla, nservicio, ncarac, nnotify);
                }
                onReleased:
                {
                    lbltres.color = "black"
                }
                
            }
        }
        Label
        {
            id: lbldos
            x: 8
            y: 61
            width: 27
            height: 29
            text: qsTr("2")
            anchors.horizontalCenterOffset: -60
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignHCenter
            font.bold: true
            font.italic: false
            font.pointSize: 17
            MouseArea
            {
                anchors.fill: parent
                onPressed:
                {
                    lbldos.color = "white"
                    //device.gattConectionGo("30:AE:A4:15:17:2A");
                    tecla = 1; intentos = 0;
                    if (fanconState == 1)
                        device.sendTeclaRf(tecla, nservicio, ncarac, nnotify);
                }
                onReleased:
                {
                    lbldos.color = "black"
                }
            }
        }
        Label
        {
            id: lbluno
            x: 37
            y: 113
            width: 27
            height: 29
            text: qsTr("1")
            anchors.verticalCenterOffset: 50
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenterOffset: -30
            anchors.horizontalCenter: parent.horizontalCenter
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            font.italic: false
            font.pointSize: 17
            MouseArea
            {
                anchors.fill: parent
                onPressed:
                {
                    lbluno.color = "white"
                    //device.gattConectionGo("30:AE:A4:15:17:2A");
                    tecla = 0; intentos = 0;
                    if (fanconState == 1)
                        device.sendTeclaRf(tecla, nservicio, ncarac, nnotify);
                }
                onReleased:
                {
                    lbluno.color = "black"
                }
            }
        }
        
        Label
        {
            id: lblcuatro
            x: 95
            y: 16
            width: 27
            height: 29
            text: qsTr("4")
            anchors.verticalCenterOffset: -50
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenterOffset: 30
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignHCenter
            font.bold: true
            font.italic: false
            font.pointSize: 17
            MouseArea
            {
                anchors.fill: parent
                onPressed:
                {
                    lblcuatro.color = "white"
                    //device.gattConectionGo("30:AE:A4:15:17:2A");
                    tecla = 3; intentos = 0;
                    if (fanconState == 1)
                        device.sendTeclaRf(tecla, nservicio, ncarac, nnotify);
                }
                onReleased:
                {
                    lblcuatro.color = "black"
                }
            }
        }
        
        Label
        {
            id: lblcinco
            x: 123
            y: 61
            width: 27
            height: 29
            text: qsTr("5")
            anchors.horizontalCenterOffset: 60
            anchors.horizontalCenter: parent.horizontalCenter
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            font.italic: false
            font.pointSize: 17
            MouseArea
            {
                anchors.fill: parent
                onPressed:
                {
                    lblcinco.color = "white"
                    //device.gattConectionGo("30:AE:A4:15:17:2A");
                    tecla = 4; intentos = 0;
                    if (fanconState == 1)
                        device.sendTeclaRf(tecla, nservicio, ncarac, nnotify);
                }
                onReleased:
                {
                    lblcinco.color = "black"
                }
            }
        }
        
        Label
        {
            id: lblseis
            x: 95
            y: 113
            width: 27
            height: 29
            text: qsTr("6")
            anchors.verticalCenterOffset: 50
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenterOffset: 30
            anchors.horizontalCenter: parent.horizontalCenter
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            font.italic: false
            font.pointSize: 17
            MouseArea
            {
                anchors.fill: parent
                onPressed:
                {
                    lblseis.color = "white"
                   // device.gattConectionGo("30:AE:A4:15:17:2A");
                    tecla = 5; intentos = 0;
                    if (fanconState == 1)
                        device.sendTeclaRf(tecla, nservicio, ncarac, nnotify);
                }
                onReleased:
                {
                    lblseis.color = "black"
                }
            }
        }
    }
    Rectangle
    {
        id: recttime
        x: 206
        width: 200
        height: 60
        color: "#ebf0f0"
        radius: 20
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 494
        
        Label
        {
            id: lbl1h
            x: 37
            y: 16
            width: 27
            height: 29
            color: "#000000"
            text: qsTr("1H")
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: 0
            font.italic: false
            anchors.horizontalCenterOffset: -51
            anchors.horizontalCenter: parent.horizontalCenter
            font.pointSize: 17
            MouseArea
            {
                anchors.fill: parent
                onPressed:
                {
                    lbl1h.color = "white"
                   // device.gattConectionGo("30:AE:A4:15:17:2A");
                    tecla = 12; intentos = 0;
                    if (fanconState == 1)
                        device.sendTeclaRf(tecla, nservicio, ncarac, nnotify);
                }
                onReleased:
                {
                    lbl1h.color = "black"
                }
            }
            
        }
        
        Label {
            id: lbl4h
            x: 43
            y: 11
            width: 27
            height: 29
            color: "#000000"
            text: qsTr("4H")
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignHCenter
            font.bold: true
            font.italic: false
            anchors.horizontalCenterOffset: 0
            anchors.horizontalCenter: parent.horizontalCenter
            font.pointSize: 17
            MouseArea
            {
                anchors.fill: parent
                onPressed:
                {
                    lbl4h.color = "white"
                    //device.gattConectionGo("30:AE:A4:15:17:2A");
                    tecla = 13; intentos = 0;
                    if (fanconState == 1)
                        device.sendTeclaRf(tecla, nservicio, ncarac, nnotify);
                }
                onReleased:
                {
                    lbl4h.color = "black"
                }
            }
            
        }
        Label
        {
            id: lbl8h
            x: 39
            y: 2
            width: 27
            height: 29
            color: "#000000"
            text: qsTr("8H")
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: 0
            font.italic: false
            anchors.horizontalCenterOffset: 51
            anchors.horizontalCenter: parent.horizontalCenter
            font.pointSize: 17
            MouseArea
            {
                anchors.fill: parent
                onPressed:
                {
                    lbl8h.color = "white"
                   // device.gattConectionGo("30:AE:A4:15:17:2A");
                    tecla = 14; intentos = 0;
                    if (fanconState == 1)
                        device.sendTeclaRf(tecla, nservicio, ncarac, nnotify);
                }
                onReleased:
                {
                    lbl8h.color = "black"
                }
            }
            
        }
    }
    Component.onCompleted:
    {
        device.startProcessBle();
    }
}

