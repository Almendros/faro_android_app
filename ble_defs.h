#ifndef BLE_DEFS_H
#define BLE_DEFS_H

#define COM_BLE_FRAME_INI     			0x01
#define COM_BLE_FRAME_FIN     			0x02

#define COM_BLE_CMD_RQ_ST     			0x0A
#define COM_BLE_CMD_CLOSE_GATT 			0x28
#define COM_BLE_INVIDENTE_PRESENTE		0x29

const char FR_STATE_RQ[3] = {COM_BLE_FRAME_INI, COM_BLE_CMD_RQ_ST, COM_BLE_FRAME_FIN};
const char FR_CLOSE_GATT[3] = {COM_BLE_FRAME_INI, COM_BLE_CMD_CLOSE_GATT, COM_BLE_FRAME_FIN};
const char FR_INVIDENTE_PRESENTE[3] = {COM_BLE_FRAME_INI, COM_BLE_INVIDENTE_PRESENTE, COM_BLE_FRAME_FIN};

//comEstadoRecepcion
#define COM_EST_RECEP_CONNECTING_GAT        0x0000001
#define COM_EST_RECEP_DISCONNECTING_GAT     0x0000002
#define COM_EST_RECEP_WAITING_2_DISCONNECT  0x0000004
//#define COM_EST_RECEP_ON                    0x0000001
//#define COM_EST_RECEP_ST                    0x0000002
//#define COM_EST_RECEP_CLOSE_GATT            0x0000004
//#define COM_EST_RECEP_CONNECTING_GAT        0x0000008
//#define COM_EST_RECEP_INV_PRES              0x0000010

//StatedlysTimer
//#define ST_DLYS_TIMER_MENSAJE_SEMAFORO      0x0000001
//#define ST_DLYS_TIMER_MENSAJE_CRUZAR        0x0000002
//#define ST_DLYS_TIMER_ESPERA_ACEPTACION     0x0000004

//typedef struct
//{
//    unsigned int new_comand;
//    unsigned int new_respuesta;
//    unsigned int comand;
//    int respuesta;

//} rec_frame_struct_t ;

//typedef enum
//{
//   BLE_NO_ANSWER  = 0,
//   BLE_OUT_INTER_RED  = 1,
//   BLE_IN_INTER_RED    = 2,
//   BLE_OUT_INTER_GREEN     = 3,
//   BLE_IN_INTER_GREEN     = 4,
//   BLE_DISCONN     = 5
//   //BLE_NO_GATT     = 6
//}BLE_AVISADOR_STATE;

typedef enum
{
   BLE_FAN_DISCON  = 0,
   BLE_FAN_CON  = 1,
   BLE_FAN_CON_ERROR    = 2
}BLE_FAN_STATE_TYP;

typedef enum
{
   BLE_SEN_CMD_RF_COM  = 0
}BLE_SEND_CMD_TYP;
typedef enum
{
   BLE_RF_PROTOCOL_0  = 0,
    BLE_RF_PROTOCOL_1  = 1,
    BLE_RF_PROTOCOL_2  = 2,
    BLE_RF_PROTOCOL_3  = 3
}BLE_RF_PROTOCOL_TYP;

#define BLE_MANDO_DIRECC_AZUL      0x1553
#define BLE_MANDO_DIRECC_OTRO      0x0AAA

typedef enum
{
    BLE_MANDO_KEY_1  = 0,
    BLE_MANDO_KEY_2  = 1,
    BLE_MANDO_KEY_3  = 2,
    BLE_MANDO_KEY_4  = 3,
    BLE_MANDO_KEY_5  = 4,
    BLE_MANDO_KEY_6  = 5,
    BLE_MANDO_KEY_POWER  = 6,
    BLE_MANDO_KEY_LIGHT_ON  = 7,
    BLE_MANDO_KEY_LIGHT_OFF  = 8,
    BLE_MANDO_KEY_FLAP  = 9,
    BLE_MANDO_KEY_FAN  = 10,
    BLE_MANDO_KEY_TEMP  = 11,
    BLE_MANDO_KEY_1H  = 12,
    BLE_MANDO_KEY_4H  = 13,
    BLE_MANDO_KEY_8H  = 14

}BLE_KEY_CODE_TYP;

#define BLE_VENT_TIPO1_KEY1        0x0202
#define BLE_VENT_TIPO1_KEY2        0x0303
#define BLE_VENT_TIPO1_KEY3        0x0404
#define BLE_VENT_TIPO1_KEY4        0x0505
#define BLE_VENT_TIPO1_KEY5        0x0606
#define BLE_VENT_TIPO1_KEY6        0x0707
#define BLE_VENT_TIPO1_POWER       0x1111
#define BLE_VENT_TIPO1_LUZ_ON      0x0000
#define BLE_VENT_TIPO1_LUZ_OFF     0x0101
#define BLE_VENT_TIPO1_FLAP        0x0909
#define BLE_VENT_TIPO1_FAN         0x0F0F
#define BLE_VENT_TIPO1_TEMP        0x0E0E
#define BLE_VENT_TIPO1_1H          0x0C0C
#define BLE_VENT_TIPO1_4H          0x0A0A
#define BLE_VENT_TIPO1_8H          0x0D0D

typedef struct
{
    unsigned int newCom;
    unsigned char nBits;
    unsigned char protocolo;
    unsigned char commBle;
    unsigned char reservado;
    unsigned int mandoDirecc;
    unsigned int commRf;
    // unsigned int chk;
}RF_send_struct_type;
//typedef struct
//{
//    unsigned int key1, key2, key3, key4, key5, key6;
//    unsigned int power, light_on, light_off, flap, fan, temp, time1h, time4h, time8h;
//}RF_cmd_tecla_type;


#endif // BLE_DEFS_H
