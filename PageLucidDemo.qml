import QtQuick 2.6
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.1

Page
{

   // property var tempvalue: device.tempvalue
    property int fanconState: device.conectionstate
    property var servicesList : device.servicesList
    property var characteristicList : device.characteristicList
    property string nservicio


    onFanconStateChanged:
    {
        if (fanconState === 0)
        {
            lblconnect.text = "DISCONNECTED";
            idButtonConnect.text = "CONNECT";
        }
        if (fanconState === 1)
        {
            lblconnect.text = "CONNECTED";
            idButtonConnect.text = "DISCONNECT";
        }
        if (fanconState === 2)
        {
            lblconnect.text = "ERROR";
            idButtonConnect.text = "CONNECT";
        }
        console.log("Fan Con State:" + fanconState);
    }
    onServicesListChanged:
    {
        var list = device.servicesList
        var keysArray = Object.keys(list);
        var longitud = keysArray.length;
        for (var j = 0; j < longitud; j++)
        {
            var str = j.toString();
            var obj = list[str];
            for (var prop in obj)
            {
                console.log(prop + ":" + obj[prop]);
                if (prop === "serviceUuid" && obj[prop] === "4880c12c-fdcb-4077-8920-a450d7f9b907")
                {
                    console.log("Tiene el service UUID que esperamos");
                    device.connectToService(obj[prop]);
                    nservicio = obj[prop];
                    break;
                }
            }
        }
    }
    onCharacteristicListChanged:
    {
        var list = device.characteristicList
        var keysArray = Object.keys(list);
        var longitud = keysArray.length;
        var servicerdy = 0;
        var ncarac = ""
        var nnotify = ""

        for (var j = 0; j < longitud; j++)
        {
            var str = j.toString();
            var obj = list[str];
            for (var prop in obj)
            {
                console.log(prop + ":" + obj[prop]);
                if (prop === "characteristicUuid" && obj[prop] === "fec26ec4-6d71-4442-9f81-55bc21d658d6")
                {
                    console.log("Tiene el characteristics UUID que esperamos");
                    nnotify = obj[prop]; ncarac = obj[prop];
                    servicerdy += 1;
                }

            }
        }
        if (servicerdy)
        {
            //con esto estamos preparados para comunicar datos con el ventilador
            device.sendTeclaRf(0, nservicio, ncarac, nnotify);
            //device.connect2FanGattNotif(nservicio, ncarac, nnotify);
        }
    }
    Label
    {
        id: lblconnect
        x: 92
        y: 94
        width: 167
        height: 25
        fontSizeMode: Text.VerticalFit
        font.pointSize: 20
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

    }

    Button
    {
        id: idButtonConnect
        x: 80
        y: 215
        width: 190
        height: 51
        onClicked:
        {
            if (fanconState === 1)
                device.disconnect2Fan();
            else
                device.gattConectionGo("30:AE:A4:15:17:2A");
        }
    }
    Frame
    {
        id: frame
        x: 70
        y: 65
        width: 212
        height: 72
    }
    Component.onCompleted:
    {
        if (fanconState === 0)
        {
            lblconnect.text = "DISCONNECTED";
            idButtonConnect.text = "CONNECT";
        }
        if (fanconState === 1)
        {
            lblconnect.text = "CONNECTED";
            idButtonConnect.text = "DISCONNECT";
        }
        if (fanconState === 2)
        {
            lblconnect.text = "ERROR";
            idButtonConnect.text = "CONNECT";
        }
        device.startProcessBle();
    }


}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
