#include "wifi.h"

#include <QtAndroidExtras/QAndroidJniObject>
#include <QtAndroid>

Wifi::Wifi()
{
    timercom = new QTimer(this);
    timercom->setSingleShot(false);
    timercom->setInterval(500);
    connect(timercom, SIGNAL(timeout()), this, SLOT(com_serial_timeout()));
    timercom->stop();


    timercheckconn = new QTimer(this);
    timercheckconn->setSingleShot(false);
    timercheckconn->setInterval(100);
    connect(timercheckconn, SIGNAL(timeout()), this, SLOT(com_check_connect()));
    timercheckconn->stop();
}
/* ******************************************************************************************** */
/**
  * @brief generamos dly para checkear la conexion
  * @param  None
  * @retval
  */
void Wifi::wifi_dly(int dly)
{
    if (dly)
    {
        timercheckconn->start(); wfconndelay = dly;
    }
    else timercheckconn->stop();
}
/* ******************************************************************************************** */
/**
  * @brief
  * @param  None
  * @retval
  */
//            QAndroidJniObject ip = QAndroidJniObject::callStaticObjectMethod("org/danfoss/WifiClient",
//                                                   "get_own_ip",
//                                                   "(I)Ljava/lang/String;",
//                                                   0);

void Wifi::com_check_connect()
{
    char cad[20];
    wfconndelay--;
    if (wfconndelay)
    {
        jint ip = QAndroidJniObject::callStaticMethod<jint>("org/danfoss/WifiClient",
                                                            "get_own_ip",
                                                            "(I)I",
                                                            0);
        if (ip)
        {
            sprintf(cad, "%d.%d.%d.%d", ip  & 0xFF, (ip >>  8) & 0xFF,(ip >> 16) & 0xFF, (ip >> 24) & 0xFF);
            wifi_Write_wfownipstr(cad);
            wifi_Write_wfconnectedst(true);
            wifi_dly(0);
        }
    }
    else
    {
        wifi_dly(0);
        wifi_Write_wfconnectedst(false);
    }

    //

    // }
    //else wfconnectedst = false;
    // emit wifi_Changed_wfconnectedst();

    //}

}
void Wifi::com_serial_timeout()
{
    wfownccid++;
    if (wfownccid > 1000) wfownccid = 0;
    emit wifi_Changed_wfownccid();
}
void Wifi::wifi_Write_wfownccid(const int &h)
{
    wfownccid = h; timercom->start();
    emit wifi_Changed_wfownccid();
}
int Wifi::wifi_Read_wfownccid(void)
{
    return wfownccid;
}
void Wifi::wifi_Write_wfnreadccids(const int &h)
{
    wfnreadccids = h;
    emit wifi_Changed_wfnreadccids();
}
int Wifi::wifi_Read_wfnreadccids(void)
{
    jint nredes = QAndroidJniObject::callStaticMethod<jint>("org/danfoss/WifiClient",
                                                            "startscan",
                                                            "(I)I", 1);
    wfnreadccids = nredes;
    qDebug() << nredes;
    ssidread.clear();
    for (int j = 0; j < nredes; j++)
    {
        QAndroidJniObject ccid = QAndroidJniObject::callStaticObjectMethod("org/danfoss/WifiClient",
                                                                           "readssid",
                                                                           "(I)Ljava/lang/String;",
                                                                           j);
        QString cad = ccid.toString();
        qDebug() << cad;
        ssidread.append(cad);
    }
    return wfnreadccids;
}
QString Wifi::wifi_CCID_Format(QString cad)
{
    QString cadret;
    int len = cad.count();
    if (len >= 2)
    {
        cadret = cad.mid(1, len - 2);
    }
    return cadret;
}
void Wifi::wifi_Write_wfownccidstr(const QString &h)
{
    wfownccidstr = h;
    emit wifi_Changed_wfownccid();
}
QString Wifi::wifi_Read_wfownccidstr(void)
{
    QAndroidJniObject context = QtAndroid::androidContext();
    //    jint ccid = QAndroidJniObject::callStaticMethod<jint>("org/danfoss/WifiClient",
    //                                           "get_ssid",
    //                                           "(Ljava/lang/String;)I",
    //                                           javaNotification.object<jstring>());

    QAndroidJniObject ccid = QAndroidJniObject::callStaticObjectMethod("org/danfoss/WifiClient",
                                                                       "get_own_ssid",
                                                                       "(I)Ljava/lang/String;",
                                                                       0);


    QString cad = ccid.toString();
    wfownccidstr =  wifi_CCID_Format(cad);
    emit wifi_Changed_wfownccidstr();
    return wfownccidstr;
}
void Wifi::wifi_Write_wfnreadccidstr(const QString &h)
{
    wfnreadccidstr = h;
    emit wifi_Changed_wfnreadccidstr();
}
QString Wifi::wifi_Read_wfnreadccidstr(void)
{
    //emit wifi_Changed_wfnreadccidstr();
    if ((wfindexccid + 1) <= ssidread.count())
        wfnreadccidstr = ssidread.at(wfindexccid);
    else
        wfnreadccidstr = "";
    return wfnreadccidstr;
}
int Wifi::wifi_Read_wfindexccid(void)
{
    return wfindexccid;
}
void Wifi::wifi_Write_wfindexccid(const int &h)
{
    wfindexccid = h;
    //emit wifi_Changed_wfindexccid();
}
bool Wifi::wifi_Read_wfconnectedst(void)
{
    return wfconnectedst;
}
void Wifi::wifi_Write_wfconnectedst(const bool &h)
{
    wfconnectedst = h;
    emit wifi_Changed_wfconnectedst();
}
/* ******************************************************************************************** */
/**
  * @brief Seleccionamos wifi a conectar
  * @param  None
  * @retval
  */
void Wifi::wifi_Connect_AP(const int &h)
{
    esp32wifiid = h + 1;
    //    for (int j = 0; j < ssidread.count(); j++)
    //    {
    //        if (ssidread.at(j).contains(h))
    //        {
    //            esp32wifiid = j + 1; break;
    //        }
    //    }
    if (esp32wifiid)
    {
        wifi_Write_wfownipstr("0.0.0.0");
        wifi_Write_wfconnectedst(false);
        jint res = QAndroidJniObject::callStaticMethod<jint>("org/danfoss/WifiClient",
                                                             "connect2wifi",
                                                             "(II)I",
                                                             (esp32wifiid - 1));
        if (res) wifi_dly(30);
    }

}
int Wifi::wifi_Read_esp32wifiid(void)
{
    return esp32wifiid;
}
void Wifi::wifi_Write_esp32wifiid(const int &h)
{
    esp32wifiid = h;
    emit wifi_Changed_esp32wifiid();
}
QString Wifi::wifi_Read_wfownipstr(void)
{
    return wfownipstr;
}
void Wifi::wifi_Write_wfownipstr(const QString &h)
{
    wfownipstr = h;
    emit wifi_Changed_wfownipstr();
}


