
import QtQuick 2.6
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.1


Pane {
    id: idbuscarredes
    signal jumpfrombuscarredes(string msg)
    padding: 0
    property bool busy
    property string ipw
    property alias petsModel  : petsModel
    property alias busywifigeneral  : busywifigeneral
    property var delegateComponentMap: {
        "ItemDelegate": itemDelegateComponent,
    }
        BusyIndicator
        {
            id: busywifigeneral
            anchors.horizontalCenter: parent.horizontalCenter
            running : false

        }
//        Label {
//            id: lblredes
//            width: 200
//            wrapMode: Label.Wrap
//            horizontalAlignment: Qt.AlignHCenter
//            text: "192.168.1.121"
//            anchors.top: parent.top
//            anchors.topMargin: 15
//            anchors.left: parent.left
//            anchors.leftMargin: 200
//            font.pixelSize: 12
//            font.bold: false
//        }
//        Button {
//            id: button
//            text: qsTr("Button")
//            anchors.top: parent.top
//            anchors.topMargin: 0
//            anchors.left: parent.left
//            anchors.leftMargin: 100
//            onClicked:
//            {
//                petsModel.append({ "text": "ItemDelegate123", ebusy: true})
//                //petsModel.append({ busy: true})
//            }
//        }

    Component
    {
        id: itemDelegateComponent

        ItemDelegate
        {
            id: itemDelegate
            text: labelText
            font.pixelSize: 14
            font.bold: true
            width: parent.width
            BusyIndicator {
                id: indisingle
                //anchors.horizontalCenter: parent.horizontalCenter
                width: 40
                height: 40
                anchors.left: parent.left
                anchors.leftMargin: 190
                running: busy
            }
            Label {
                id: ipwifi
                width: 200
                wrapMode: Label.Wrap
                horizontalAlignment: Qt.AlignHCenter
                text: ipw
                anchors.top: parent.top
                anchors.topMargin: 15
                anchors.left: parent.left
                anchors.leftMargin: 200
                font.pixelSize: 12
                font.bold: false
            }
            onClicked:
            {
                for (var j = 0; j < view.count; j++)
                {
                    if (petsModel.get(j).text === itemDelegate.text)
                    {
                        console.log(j);
                        petsModel.set(j, {"ebusy": true, "eipw": "0.0.0.0"});
                        idbuscarredes.jumpfrombuscarredes(j);
                       // lastwificlicked = j + 1;
                        break;
                    }
                }
            }
        }
    }//component

    ColumnLayout
    {
        id: column
        spacing: 40
        anchors.fill: parent
        anchors.topMargin: 60

        ListView {
            id: listView
            Layout.fillWidth: true
            Layout.fillHeight: true
            clip: true
            model: ListModel {
                id: petsModel
//                ListElement { type: "ItemDelegate"; text: "ItemDelegate1" ; ebusy: true}
//                ListElement { type: "ItemDelegate"; text: "ItemDelegate2" ; ebusy: true}
//                ListElement { type: "ItemDelegate"; text: "ItemDelegate3" ; ebusy: true}
            }

            section.property: "type"
            section.delegate: Pane {
                width: listView.width
            }

            delegate: Loader {
                id: delegateLoader
                width: listView.width
                sourceComponent: itemDelegateComponent

                property string labelText: text
                property bool busy: ebusy
                property string ipw: eipw
                property ListView view: listView
                property int ourIndex: index

                // Can't find a way to do this in the SwipeDelegate component itself,
                // so do it here instead.
                ListView.onRemove: SequentialAnimation {
                    PropertyAction {
                        target: delegateLoader
                        property: "ListView.delayRemove"
                        value: true
                    }
                    NumberAnimation {
                        target: item
                        property: "height"
                        to: 0
                        easing.type: Easing.InOutQuad
                    }
                    PropertyAction {
                        target: delegateLoader
                        property: "ListView.delayRemove"
                        value: false
                    }
                }
            }
        }
    }
}
